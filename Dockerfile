# Use an official python3 image 
FROM python:3 

# set the working directory of container to app 
WORKDIR /app

# copy the contents of /wine-not/app to /app of container
COPY ./app /app

# copy the requirements file to current working directory as well
COPY requirements.txt ./

# install the needed packages
RUN pip install -r requirements.txt

# make port 8000 available to the world outside of the container 
EXPOSE 8000 

# commands to run the file when we launch container
CMD ["python", "server.py"]
