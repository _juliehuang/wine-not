
# Wine-not

Try our live [Wine app demo](http://54.200.231.212/)

A exploratory analysis of fine wines from a [UCI's Machine Learning Repository](https://archive.ics.uci.edu/ml/datasets/wine+quality)

Inspiration: Use machine learning to determine which physiochemical properties make a wine 'good'! - Predict the `quality` of a wine 
- Maybe incorporate an arbitrary "cut-off" for quality - i.e. 8 and above is 'good'

Aside: 
It is important to take note that model is trained using an extremely biased data set (i.e., the majority of wines are rated 5 or 6). Hence, predictions will not be as accurate for wines that are on the lower end (rated < 4) or higher end because the data is lacking in these types of wines.

## Usage: 
To run the flask app locally first navigate to the `app` directory. 

In order to run the app, you first need to tell your terminal which application it will run. You will need to export the `FLASK_APP`: 
```bash 
export FLASK_APP=server.py
```

You would also want to turn on debug mode. Otherwise, you would have to restart the flask app every time you make changes. 
```bash 
export FLASK_DEBUG=1
```
Finally, type in `flask run` to run the app locally! 

One can also pull the Docker image by running the following: 
```bash
docker run -p 4000:8000 juliehuang/wine-app:1.0
```
