#import libraries
import numpy as np
import pickle
import flask
from flask import Flask, render_template, request, redirect, url_for

# create instance of the flask app
app = Flask(__name__)

# prediction function


def predict(input_data):
    to_predict = np.array(input_data).reshape(1, 8)
    model = pickle.load(open('model.pkl', 'rb'))
    result = model.predict(to_predict)
    return result[0]


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/result', methods=['POST', 'GET'])
def result():
    if request.method == 'POST':
        input_data = request.form.to_dict()
        input_data = list(input_data.values())
        input_data = list(map(float, input_data))

        result = predict(input_data)
        return render_template("result.html", prediction=result)

    if request.method == 'GET':
        return redirect(url_for('index'))


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8000)
