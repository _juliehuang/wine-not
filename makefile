run: install nginx server

.PHONY: nginx

install:
	sudo apt install nginx
	wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
	bash Miniconda3-latest-Linux-x86_64.sh

nginx:
	[ -f  /etc/nginx/sites-enabled/default ] && sudo rm /etc/nginx/sites-enabled/default || echo file does not exist
	sudo cp nginx /etc/nginx/sites-enabled/
	sudo /etc/init.d/nginx restart

server:
	python3 app/server.py
